#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/Kernel/global_functions.h>

#include <iostream>
#include <fstream>
#include <random>
#include <string>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef CGAL::Vector_3<Kernel> Vector_3;
typedef CGAL::Direction_3<Kernel> Direction_3;

typedef Polyhedron::Facet_const_iterator Facet_iterator;
typedef Polyhedron::Vertex_const_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_const_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_const_circulator Halfedge_facet_circulator;

typedef std::map<Polyhedron::Facet_const_handle, double> Facet_const_double_map;
typedef std::map<Polyhedron::Facet_const_handle, int> Facet_const_int_map;

typedef std::map<Polyhedron::Facet_handle, double> Facet_double_map;
typedef std::map<Polyhedron::Facet_handle, int> Facet_int_map;

struct ClassColor {
	double r, g, b;
};

enum ColorizeMode {Gradient, Random};

enum ThresholdingMode {Perim, Area, Angle};

/// @brief Remplit un vecteur de couleurs générées aléatoirement
/// @param nbClasses Nombre de couleurs à générer
/// @param out Vecteur à remplir
void generateRandomClassColors(int nbClasses, std::vector<ClassColor>& out) {
	// Initialisation de la seed
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0, 1);

	// Génération d'une couleur aléatoire pour chaque classe
	for (int i = 0; i < nbClasses; i++)
	{
		out.push_back({dis(gen), dis(gen), dis(gen)});
	}

	//std::cout << nbClasses << std::endl;
}

/// @brief map all the values from [min, max] to [0, 1]
/// @param facetMap non-const reference to the map (it is an in/out parameter)
void normalizeMap(Facet_const_double_map &facetMap)
{
	double maxValue = facetMap.begin()->second;
	double minValue = facetMap.begin()->second;

	// look for min and max value in the map
	for (const auto &elem : facetMap)
	{
		if (elem.second > maxValue)
		{
			maxValue = elem.second;
		}
		if (elem.second < minValue)
		{
			minValue = elem.second;
		}
	}

	for (auto &elem : facetMap)
	{
		elem.second -= minValue;
		elem.second /= (maxValue-minValue);
		if (isnan(elem.second)) elem.second = 0;
	}
}

/// @brief Colorie une facette dans un fichier .off avec une couleur entre le vert et le rouge en fonction de son numéro de classe
/// @param in_myfile Le fichier .off. Cette fonction ne se cahrge pas de l'ouverture/fermeture du fichier.
/// @param facetMap La map des classes de chaque face
/// @param i L'itérateur sur la facette à colorier
void colorizeFacetGradient(std::ofstream& in_myfile, const Facet_const_double_map& facetMap, Facet_iterator i) {
	if (in_myfile.is_open()) {
		auto redValue = 1-facetMap.at(i); // low values will be closer to red
		auto greenValue = facetMap.at(i); // high values will be closer to green
		auto blueValue = 0.0;

		in_myfile << " " << redValue << " " << greenValue << " " << blueValue;
	}
}

/// @brief Colorie une facette dans un fichier .off avec la couleur correspondante dans le tableau de couleurs donné en paramètres
/// @param in_myfile Le fichier .off. Cette fonction ne se cahrge pas de l'ouverture/fermeture du fichier
/// @param facetMap La map des classes de chaque face
/// @param i L'itérateur sur la facette à colorier
/// @param colors Le tableau de couleurs. Supposé généré aléatoirement.
void colorizeFacetRandom(std::ofstream& in_myfile, const Facet_const_double_map& facetMap, Facet_iterator i, const std::vector<ClassColor>& colors) {
	if (in_myfile.is_open()) {
		auto redValue = colors[facetMap.at(i)-1].r;
		auto greenValue = colors[facetMap.at(i)-1].g;
		auto blueValue = colors[facetMap.at(i)-1].b;

		//std::cout << "(" << redValue << ", " << greenValue << ", " << blueValue << ")" << std::endl;

		in_myfile << " " << redValue << " " << greenValue << " " << blueValue;
	}
}

/// @brief Generate in a .off file a colored mesh according to a value map (green to red shades)
/// @param mesh the input mesh
/// @param facetMap map of values between 0 and 1 (see "normalize()") for each facet of mesh
/// @param filePath path to the colored .off file to be generated
void writeOFFfromValueMap(const Polyhedron& mesh, Facet_const_double_map& facetMap, std::string filePath, ColorizeMode colorMode, std::vector<ClassColor>* colors = nullptr)
{
	std::cout << "Ecriture du résultat dans " << filePath << " ..." << std::endl;

	std::ofstream in_myfile;
	in_myfile.open(filePath);

	CGAL::set_ascii_mode(in_myfile);

	in_myfile << "COFF" << std::endl // "COFF" makes the file support color informations
			  << mesh.size_of_vertices() << ' ' 
			  << mesh.size_of_facets() << " 0" << std::endl; 
			  // nb of vertices, faces and edges (the latter is optional, thus 0)

	std::copy(mesh.points_begin(), mesh.points_end(),
			  std::ostream_iterator<Kernel::Point_3>(in_myfile, "\n"));

	if (colorMode == ColorizeMode::Gradient) normalizeMap(facetMap);

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		Halfedge_facet_circulator j = i->facet_begin();

		CGAL_assertion(CGAL::circulator_size(j) >= 3);

		in_myfile << CGAL::circulator_size(j) << ' ';
		do
		{
			in_myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());

		} while (++j != i->facet_begin());

		in_myfile << std::setprecision(5) << std::fixed; //set the format of floats to X.XXXXX

		switch (colorMode)
		{
		case ColorizeMode::Gradient:
			colorizeFacetGradient(in_myfile, facetMap, i);
			break;
		case ColorizeMode::Random:
			if (colors != nullptr) 
				colorizeFacetRandom(in_myfile, facetMap, i, *colors);
			else 
				std::cout << "Erreur : Impossible de colorier correctement le maillage. Veuillez donner un tableau de couleurs en paramètre à la fonction writeOFFfromValueMap()" << std::endl;
			break;
		}

		in_myfile << std::endl;
	}

	in_myfile.close();

	std::cout << "Le résultat a été exporté dans " << filePath << " !" << std::endl;
}

/// @brief Calcule le périmètre de chaque face d'un mesh
/// @param mesh Le mesh en entrée
/// @return La map des paires face/périmètre
Facet_const_double_map computePerimMap(const Polyhedron &mesh)
{
	Facet_const_double_map out;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		double current_perimeter = 0.;
		Halfedge_facet_circulator j = i->facet_begin();
		do
		{
			current_perimeter += std::sqrt(CGAL::squared_distance(j->vertex()->point(), j->opposite()->vertex()->point()));
		} while (++j != i->facet_begin());

		//std::cout << "perim(" << std::distance(mesh.facets_begin(), i) << ")=" << current_perimeter << std::endl;

		out[i] = current_perimeter;
	}

	return out;
}

/// @brief Calcule l'aire de chaque face d'un mesh
/// @param mesh Le mesh en entrée
/// @return La map des paires face/aire
Facet_const_double_map computeAreaMap(const Polyhedron &mesh)
{
	Facet_const_double_map out;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
	
		Halfedge_facet_circulator j = i->facet_begin();
		
		Polyhedron::Vertex_const_handle firstVertex = j->vertex();

		double current_area = 0;
		// a facet is not necessarily a triangle, so we decompose one facet into multiple triangles,
		// and sum up all their areas. Only works for convex faces.
		// (illustration: http://mathbitsnotebook.com/JuniorMath/Polygons/polygons3g.jpg)
		do
		{
			current_area += CGAL::squared_area(firstVertex->point(), j->vertex()->point(), j->opposite()->vertex()->point()); 
		} while (++j != i->facet_begin());

		//std::cout << "area(" << std::distance(mesh.facets_begin(), i) << ")=" << current_area << std::endl;

		out[i] = current_area;
	}

	return out;
}

/// @brief Calcule, pour chaque face d'un mesh, l'angle entre la normale de la face et le vecteur de référence
/// @param mesh Le mesh en entrée
/// @param referenceAngle Le second vecteur utilisé pour calculer l'angle
/// @return La map des paires face/angle
Facet_const_double_map computeNormalAngleMap(const Polyhedron &mesh, Vector_3 referenceAngle)
{
	Facet_const_double_map out;

	Vector_3 currentNormal;
	double currentAngle;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		Halfedge_facet_circulator j = i->facet_begin();

		currentNormal = CGAL::normal(j->vertex()->point(), j->next()->vertex()->point(), j->prev()->vertex()->point());
		currentAngle = CGAL::approximate_angle(currentNormal, referenceAngle);
		//std::cout << "normal(" << std::distance(mesh.facets_begin(), i) << ")=" << currentNormal << std::endl;
		//std::cout << "angle(" << std::distance(mesh.facets_begin(), i) << ")=" << currentAngle << std::endl;

		out[i] = currentAngle;
	}

	return out;
}

/// @brief Retourne vrai si la valeur de left est inférieure à celle de right
/// @param left La première valeur à comparer
/// @param right La seconde valeur à comparer
/// @return Résultat de la comparaison
bool compareValueInf(const std::pair<Polyhedron::Facet_const_handle, double>& left, const std::pair<Polyhedron::Facet_const_handle, double>& right)
{
	return left.second < right.second;
}

/// @brief Retourne vrai si la valeur de left est supérieure à celle de right
/// @param left La première valeur à comparer
/// @param right La seconde valeur à comparer
/// @return Résultat de la comparaison
bool compareValueSup(const std::pair<Polyhedron::Facet_const_handle, double>& left, const std::pair<Polyhedron::Facet_const_handle, double>& right)
{
	return left.second > right.second;
}

/// @brief Donne la valeur minimale ou maximale dans une map de paires face/valeur
/// @param facetMap La map en entrée
/// @param comp Le comparateur à utiliser
/// @return La valeur min ou max
double getMinOrMaxValueInFacetMap(const Facet_const_double_map& facetMap, bool (*comp)(const std::pair<Polyhedron::Facet_const_handle, double>&, const std::pair<Polyhedron::Facet_const_handle, double>&)) 
{
  std::pair<Polyhedron::Facet_const_handle, double> min = *min_element(facetMap.begin(), facetMap.end(), comp);
  return min.second; 
}

/// @brief Effectue un seuillage pour classer les facettes d'un maillage
/// @param mesh Le maillage en entrée
/// @param nbClasses Le nombre de classes souhaitées
/// @param mode La valeur à utiliser pour faire le seuillage : aire de la face, périmètre, ou angle avec la normale
/// @return La map de paires face/classe
Facet_const_double_map facetsThresholding(const Polyhedron &mesh, int nbClasses, ThresholdingMode mode) {
    Facet_const_double_map out, mapValue;
	
	double minValue, maxValue;

	switch (mode)
	{
	case ThresholdingMode::Perim:
		mapValue = computePerimMap(mesh);
		minValue = getMinOrMaxValueInFacetMap(mapValue, compareValueInf);
		maxValue = getMinOrMaxValueInFacetMap(mapValue, compareValueSup);
		break;
	case ThresholdingMode::Area:
		mapValue = computeAreaMap(mesh);
		minValue = getMinOrMaxValueInFacetMap(mapValue, compareValueInf);
		maxValue = getMinOrMaxValueInFacetMap(mapValue, compareValueSup);
		break;
	case ThresholdingMode::Angle:
		Vector_3 X_axis = Vector_3(1.0,0.0,0.0);
		Vector_3 Y_axis = Vector_3(0.0,1.0,0.0);
		Vector_3 Z_axis = Vector_3(0.0,0.0,1.0);

		mapValue = computeNormalAngleMap(mesh, Y_axis);
		minValue = 0;
		maxValue = 180;
		break;
	}
	
	// Calcul des différents seuils
	double classSpan = (maxValue - minValue) / nbClasses;
	double thresholds[nbClasses-1];
	for (int k = 0; k < nbClasses; k++)
	{
		thresholds[k] = minValue + classSpan * (k+1);
	}

	//std::cout << thresholds[0] << std::endl;
    for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		int current_class = nbClasses;
		
		while (mapValue[i] <= thresholds[current_class-1] && current_class > 0)
		{
			current_class--;
		}
		//if (current_class < 0) current_class = nbClasses - 1;
		
        out[i] = current_class + 1;

        std::cout << "classe(" << std::distance(mesh.facets_begin(), i) << ")=" << out[i] << std::endl;
    }
    return out;
}

/// @brief Effectue un parcours en profondeur à partir d'une face donnée sur un mesh donné
/// @param mesh Le mesh en entrée
/// @param segmentation La segmentation résultat (utilisé comme une sortie de la fonction)
/// @param i Itérateur sur la face à explorer
/// @param tableau_parcours Tableau des faces déjà explorées
/// @param nouv_classe Classe de la face après parcours en profondeur
/// @param prec_classe Classe de la face avant parcours en profondeur
void Parcours_Profond(Polyhedron & mesh, Facet_const_double_map & segmentation, Facet_iterator i, bool * tableau_parcours, double & nouv_classe, double prec_classe){
	Facet_iterator face_deb = mesh.facets_begin();
	if(!tableau_parcours[std::distance(face_deb,i)]){

		if(segmentation[i]==prec_classe){
			tableau_parcours[std::distance(face_deb,i)] = true;
			//std::cout<<"Face("<<std::distance(face_deb, i)<<") est identique à son voisin"<<std::endl;
			
			segmentation[i]=nouv_classe;
			std::cout<< "classeCC(" << std::distance(face_deb, i) << ")=" <<segmentation[i]<<std::endl;
			Halfedge_facet_circulator j = i->facet_begin();
			
			do{
				Parcours_Profond(mesh,segmentation, j->opposite()->facet(),tableau_parcours,nouv_classe,prec_classe);	
			}while(++j !=  i->facet_begin());
		}
		else{
			//std::cout<<"Face("<<std::distance(face_deb, i)<<") est différent de son voisin"<<std::endl;
		}
	}
}

/// @brief Effectue une seconde segmentation (après seuillage) pour séparer les composantes non-connexes de même classe
/// @param mesh Le mesh en entrée
/// @param segmentation La précédente segmentation (seuillage)
/// @param nbClasses Le nouveau nombre de classes segmentationParCC
/// @return La nouvelle map de paires face/classe
Facet_const_double_map segmentationParCC(Polyhedron & mesh, Facet_const_double_map segmentation, int* nbClasses){
	Facet_const_double_map out = segmentation;

	bool tableau_parcours[mesh.size_of_facets()] = {false};
	
	Facet_iterator face_deb = mesh.facets_begin();

	double current_class = segmentation[face_deb];
	double nouv_classe = getMinOrMaxValueInFacetMap(segmentation,compareValueInf);

	for(Facet_iterator i = mesh.facets_begin();i!=mesh.facets_end();++i){
		
		if(!tableau_parcours[std::distance(face_deb,i)]){
			nouv_classe++;
			current_class = segmentation[i];
			Parcours_Profond(mesh,out,i,tableau_parcours, nouv_classe, current_class);
		}
	}

	*nbClasses = nouv_classe > (current_class) ? nouv_classe : current_class;
	return out;
}

/// @brief Programme principal
/// @param argc Nombre d'arguments passés au programme
/// @param argv Arguments passés au programme
/// @return Code d'erreur du programme
int main(int argc, char *argv[])
{
	if (argc < 4)
	{
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off, puis le nombre de classes souhaitées pour la segmentation, puis le type de segmentation souhaité (perim / area / angle)." << std::endl;
		return 1;
	}

	Polyhedron mesh;

	std::ifstream input(argv[1]);

	if (!input || !(input >> mesh) || mesh.is_empty())
	{
		std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
		return 1;
	}

	// Conversion du paramètre nbClasses en int
	std::istringstream ss(argv[2]);
	int nbClasses;
	if (!(ss >> nbClasses) || !ss.eof()) {
		std::cerr << "Le nombre de classes donné est invalide : " << argv[2] << '\n';
		return 1;
	}

	ThresholdingMode thresholdingMode;
	std::string arg3(argv[3]);
	if (arg3 == "perim") {
		thresholdingMode = ThresholdingMode::Perim;
	}
	else if (arg3 == "area") {
		thresholdingMode = ThresholdingMode::Area;
	}
	else if (arg3 == "angle") {
		thresholdingMode = ThresholdingMode::Angle;
	}
	else {
		std::cerr << "Le mode de segmentation donné est invalide : " << arg3 << '\n';
		std::cerr << "Les valeurs possibles sont : (perim / area / angle).\n";
		return 1;
	}
	
	auto mapClasses = facetsThresholding(mesh, nbClasses, thresholdingMode);

	auto mapClassesCC = segmentationParCC(mesh, mapClasses, &nbClasses);
	std::vector<ClassColor> colors;
	generateRandomClassColors(nbClasses, colors);
	writeOFFfromValueMap(mesh, mapClassesCC, argc>=5?argv[4]:"result.off", ColorizeMode::Random, &colors);

	return 0;
}